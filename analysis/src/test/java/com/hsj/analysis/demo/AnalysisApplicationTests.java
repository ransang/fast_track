package com.hsj.analysis.demo;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AnalysisApplicationTests {

	@Test
	public void contextLoads() throws IOException {
		String address = "http://clipwallet.co.kr/";
		Document doc = Jsoup.connect(address).header("User-Agent", "Mozilla/5.0").get();
		//System.out.println(doc);
		
		Elements links = doc.select("a[href]");
		Elements metas = doc.select("meta[content]");
		Elements areas = doc.select("area[href]");
				
		for (Element src : areas)
		{	
			String url    = src.attr("href");
			String absUrl = src.attr("abs:href");
			
			//linkSave(url, absUrl);
		}
		

		
		for (Element src : metas)
		{
			String link = src.attr("content");
			String [] split = link.split("=");
			
			if(split.length >= 2)
			{
				String eUrl = link.split("=")[1];
				//linkSave(eUrl,eUrl);
			}
			
		}
				
		for (Element link : links)
		{		
			String url    = link.attr("href");
			String absUrl = link.attr("abs:href");
			
			System.out.println(url);
			System.out.println(absUrl);
		}

	}

}
