<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="com.hsj.analysis.demo.domain.BbsDomain"%>

<%
	//String bbsInfo = (String)request.getAttribute("bbsInfo");
	List<BbsDomain> bbsList = (List<BbsDomain>) request.getAttribute("bbsList");
%>
<%-- <%
	String result = (String)request.getAttribute("result");
%> --%>
<html>
<head>
<link rel="stylesheet" href="/css/style.css">
<link href="/css/jquery-ui.css" rel="stylesheet" />
<link href="/css/tree_ui.css" rel="stylesheet" />

<!-- treeview -->
<link rel="stylesheet" href="/css/web-icons.min.css">
<link rel="stylesheet" href="/css/bootstrap-treeview.css">
<link rel="stylesheet" href="/css//bootstrap.min.css">
<link rel="stylesheet" href="/css/custom.css">

<script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.js"></script>

<script language="javascript">
	$(function() {
		$("#btn01").click(function() {
			//alert(result);
			var f = document.frm;
			f.method = "get";
			f.action = "/getLayout";
			f.submit();
			//self.location.href = "/getLayout";
		});
	});
</script>
</head>
<body>
	<div class="body50">
		<p class="body_title" style="margin-left: 10px; margin-top: 10px;">
			<img src="/images/bullet01.gif">글 등록
		</p>
	</div>
	<br>
	<br>

	<div class="magT15">
		<form name="frm" action="/getLayout" method="post">
			<table border="0" cellspacing="0" width="250px" class="dataTable01">
				<tbody>
					<tr>
						<td class="tlbtop01">url</td>
						<td class="tlbtop01">headerVal1</td>
						<td class="tlbtop01">headerVal2</td>
					</tr>
					<tr>
						<td class="tlb03"><input type="text" name="address"
							id="address" class="input" style="width: 40%" /></td>
						<td class="tlb03"><input type="text" name="headerVal1"
							id="headerVal1" class="input" style="width: 40%" /></td>
						<td class="tlb03"><input type="text" name="headerVal2"
							id="headerVal2" class="input" style="width: 40%" /></td>
					</tr>
				</tbody>
			</table>
			<center>
				<input type="submit" class="btn bt_red"
					style="margin-top: 5px; margin-bottom: 5px;" value="접근성 검증" />
			</center>
		</form>
	</div>
</body>
</html>

