<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.hsj.analysis.demo.domain.ResourceDomain"%>

<%
	Map<String, HashMap<String, String>> resources = (Map<String, HashMap<String, String>>) request
			.getAttribute("resources");
	int altP = (Integer) request.getAttribute("altP");
	ResourceDomain resourceDomain = (ResourceDomain) request.getAttribute("resourceDomain");
%>
<%-- <%
	String result = (String)request.getAttribute("result");
%> --%>
<html>
<head>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.jqplot.js"></script>
<script type="text/javascript" src="/js/plugins/jqplot.barRenderer.js"></script>
<script type="text/javascript" src="/js/plugins/jqplot.pieRenderer.js"></script>
<script type="text/javascript"
	src="/js/plugins/jqplot.categoryAxisRenderer.js"></script>
<script type="text/javascript" src="/js/plugins/jqplot.pointLabels.js"></script>
<link rel="stylesheet" type="text/css" href="/js/jquery.jqplot.css" />



<link rel="stylesheet" href="/css/style.css">
<link href="/css/jquery-ui.css" rel="stylesheet" />
<link href="/css/tree_ui.css" rel="stylesheet" />


<link rel="stylesheet" href="/css/web-icons.min.css">
<link rel="stylesheet" href="/css/bootstrap-treeview.css">
<link rel="stylesheet" href="/css//bootstrap.min.css">
<link rel="stylesheet" href="/css/custom.css">

<script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.js"></script>

<script language="javascript">
	$(function() {
		$("#btn01").click(function() {
			//alert(result);
			/* var f = document.frm;
			f.method = "get";
			f.action = "/rest/bbs/getBbsInfoList";
			f.submit(); */
			self.location.href = "/search/getSearchBbsInfoList";
		});
	});

	$(document).ready(
			function() {
				$.jqplot.config.enablePlugins = true;
				var s1 = [<%=altP%>, 6, 7, 10, 1, 1, 2, 3, 4, 5 ];
				var ticks = [ '대체텍트스', '제목제공', '제목제공', '기본언어명시', '새창알림사전공지',
						'레이블제공', '표의구성', '적절한링크텍스트', '건너뛰기링크', '마크업문법' ];

				plot1 = $.jqplot('chart1', [ s1 ], {
					// Only animate if we're not using excanvas (not in IE 7 or IE 8)..
					animate : !$.jqplot.use_excanvas,
					seriesDefaults : {
						renderer : $.jqplot.BarRenderer,
						pointLabels : {
							show : true
						}
					},
					axes : {
						xaxis : {
							renderer : $.jqplot.CategoryAxisRenderer,
							ticks : ticks
						},
						yaxis : {
							min : 0,
							max : 100
						}
					},
					highlighter : {
						show : false
					}
				});

				$('#chart1').bind(
						'jqplotDataClick',
						function(ev, seriesIndex, pointIndex, data) {
							$('#info1').html(
									'series: ' + seriesIndex + ', point: '
											+ pointIndex + ', data: ' + data);
						});
			});
</script>
</head>
<body>

	<div class="body50">
		<p class="body_title" style="margin-left: 10px; margin-top: 10px;">
			<img src="/images/bullet01.gif">접근성 준수율
		</p>
	</div>
	<br>
	<br>
	<br>

	<div id="chart1" class="magT15"></div>
	<br>
	<br>


	<div class="body50">
		<p class="body_title" style="margin-left: 10px; margin-top: 10px;">
			<img src="/images/bullet01.gif">대체 텍스트 정보
		</p>
	</div>
	<br>
	<br>

	<div class="magT15">
		<table border="1" cellspacing="0" width="500px" class="dataTable01">
			<tbody>
				<tr>
					<th class="tlbtop01">이미지</th>
					<th class="tlbtop01">이미지 경로</th>
					<th class="tlbtop01">대체 텍스트</th>
				</tr>

				<%
					for (int i = 0; i < resources.get("imgs_img").size(); i++) {
				%>
				<tr>
					<td class="tlb01">
						<%
							if (resources.get("imgs_img").get("imgs_img_" + i).indexOf("http") > 0) {
						%>
						<h2>
							<img
								src="<%=resourceDomain.getAddress()%><%=resources.get("imgs_img").get("imgs_img_" + i)%>"
								width=50px, height=50px />
						</h2> <%
								} else {
								%>
						<h2>
							<img src="<%=resources.get("imgs_img").get("imgs_img_" + i)%>"
								width=50px, height=50px />
						</h2> <%
							 	}
							 %>
					</td>
					<td class="tlb01"><%=resources.get("imgs_img").get("imgs_img_" + i)%></td>
					<td class="tlb01"><%=resources.get("imgs_alt").get("imgs_alt_" + i)%></td>
				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>
</body>
</html>

