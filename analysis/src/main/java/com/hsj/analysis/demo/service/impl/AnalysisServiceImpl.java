package com.hsj.analysis.demo.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hsj.analysis.demo.dao.AnalysisDao;
import com.hsj.analysis.demo.domain.BbsDomain;
import com.hsj.analysis.demo.domain.ResourceDomain;
import com.hsj.analysis.demo.service.AnalysisService;
import com.hsj.analysis.demo.util.WebResourceProvider;

@Service
public class AnalysisServiceImpl implements AnalysisService{

	@Autowired
	private AnalysisDao analysisDao;
	
	@Autowired
	private WebResourceProvider webResourceProvider;

	@Override
	public List<BbsDomain> getBbsList() throws Exception {
		// TODO Auto-generated method stub
		return analysisDao.getBbsList();
	}

	@Override
	public Map<String, HashMap<String, String>> getAnlysisInfo(ResourceDomain resourceDomain) throws Exception {
		// TODO Auto-generated method stub
		return webResourceProvider.getWebResouces(resourceDomain);
	}
	
	
}
