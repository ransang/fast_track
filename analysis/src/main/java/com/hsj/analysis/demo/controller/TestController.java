package com.hsj.analysis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsj.analysis.demo.domain.BbsDomain;
import com.hsj.analysis.demo.service.AnalysisService;

@RestController
public class TestController {
	@Autowired
	AnalysisService analysisService;

	@RequestMapping("/getHello")
	public List<BbsDomain> index() throws Exception {
		System.out.println("test : "+analysisService.getBbsList());
		return analysisService.getBbsList();
	}
}