package com.hsj.analysis.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hsj.analysis.demo.domain.BbsDomain;
import com.hsj.analysis.demo.domain.ResourceDomain;
import com.hsj.analysis.demo.service.AnalysisService;

@Controller
public class TestController2 {
	@Autowired
	AnalysisService analysisService;

	@RequestMapping(value="/getBbsList", method = RequestMethod.GET)
	public String getHello(BbsDomain Domain, Model model, HttpServletRequest req) throws Exception {
		System.out.println("test : "+analysisService.getBbsList());
		List<BbsDomain> bbsList = (List<BbsDomain>) analysisService.getBbsList();
		model.addAttribute("bbsList", bbsList);
		return "/bbs/result";
	}
	
	@RequestMapping(value="/getLayout", method = RequestMethod.POST)
	public String getLayout(ResourceDomain resourceDomain, Model model, HttpServletRequest req) throws Exception {
		//System.out.println("test : "+analysisService.getBbsList());
		Map<String, HashMap<String, String>> resources = (Map<String, HashMap<String, String>>) analysisService.getAnlysisInfo(resourceDomain);
		
		int altP = 0;
		int totalImgCnt = resources.get("imgs_img").size();
		int totalAltCnt = 0;
		
		for(int i = 0 ; i < resources.get("imgs_alt").size() ; i ++) {
			if(!resources.get("imgs_alt").get("imgs_alt_"+i).equals("null")) {
				totalAltCnt += 1;
			}
		}
		
		if(totalImgCnt != 0 && totalAltCnt != 0) {
			altP = (int)((totalAltCnt * 100 / totalImgCnt)) ;
		}
		
		model.addAttribute("resources", resources);
		model.addAttribute("altP", altP);
		model.addAttribute("resourceDomain", resourceDomain);
		return "/bbs/result2";
	}
}