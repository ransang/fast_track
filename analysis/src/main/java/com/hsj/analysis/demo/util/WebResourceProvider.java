package com.hsj.analysis.demo.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.hsj.analysis.demo.domain.ResourceDomain;

public class WebResourceProvider {
	
	public Map<String, HashMap<String, String>> getWebResouces(ResourceDomain resourceDomain){
		Map<String, HashMap<String, String>> resources = new HashMap<String, HashMap<String, String>>();
		
		//String address = "http://clipwallet.co.kr/";
		String address = resourceDomain.getAddress();
		Document doc = null;
		try {
			//doc = Jsoup.connect(address).header("User-Agent", "Mozilla/5.0").get();
			doc = Jsoup.connect(address).header(resourceDomain.getHeaderVal1(), resourceDomain.getHeaderVal2()).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(doc);
		
		Elements imgs = doc.select("img");
		Elements links = doc.select("a[href]");
		Elements metas = doc.select("meta[content]");
		Elements areas = doc.select("area[href]");
		
		HashMap<String, String> imgMap = new HashMap<String, String>();
		HashMap<String, String> altMap = new HashMap<String, String>();
		for (int i = 0 ; i < imgs.size() ; i ++)
		{		
			String img    = imgs.get(i).attr("src");
			String alt    = imgs.get(i).attr("alt");
			
			imgMap.put("imgs_img_"+i, img);
			if(alt.equals("") || alt.length() == 0 || alt == null) {
				altMap.put("imgs_alt_"+i, "null");
			}else {
				altMap.put("imgs_alt_"+i, alt);
			}
		}
		resources.put("imgs_img", imgMap);
		resources.put("imgs_alt", altMap);
		
		for (Element src : areas)
		{	
			String url    = src.attr("href");
			String absUrl = src.attr("abs:href");
			
			//linkSave(url, absUrl);
		}
		

		
		for (Element src : metas)
		{
			String link = src.attr("content");
			String [] split = link.split("=");
			
			if(split.length >= 2)
			{
				String eUrl = link.split("=")[1];
				//linkSave(eUrl,eUrl);
			}
			
		}
				
		for (int i = 0 ; i < links.size() ; i ++)
		{		
			Element element = links.get(i);
			String url    = element.attr("href");
			String absUrl = element.attr("abs:href");
			
			//System.out.println(url);
			//System.out.println(absUrl);
			/*resources.put("links_href_"+i, url);
			resources.put("links_abs:href_"+i, absUrl);*/
		}
		return resources;
	}
}
