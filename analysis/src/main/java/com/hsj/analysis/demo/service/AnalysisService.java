package com.hsj.analysis.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hsj.analysis.demo.domain.BbsDomain;
import com.hsj.analysis.demo.domain.ResourceDomain;

public interface AnalysisService {
	public List<BbsDomain> getBbsList() throws Exception;
	public Map<String, HashMap<String, String>> getAnlysisInfo(ResourceDomain resourceDomain) throws Exception;
}
