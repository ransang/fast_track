package com.hsj.analysis.demo.domain;

public class ResourceDomain {
	private String address;
	private String headerVal1;
	private String headerVal2;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getHeaderVal1() {
		return headerVal1;
	}
	public void setHeaderVal1(String headerVal1) {
		this.headerVal1 = headerVal1;
	}
	public String getHeaderVal2() {
		return headerVal2;
	}
	public void setHeaderVal2(String headerVal2) {
		this.headerVal2 = headerVal2;
	}
	
	
}
