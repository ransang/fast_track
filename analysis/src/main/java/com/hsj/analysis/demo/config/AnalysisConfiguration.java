package com.hsj.analysis.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.tiles3.TilesView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import com.hsj.analysis.demo.service.AnalysisService;
import com.hsj.analysis.demo.service.impl.AnalysisServiceImpl;
import com.hsj.analysis.demo.util.WebResourceProvider;

@Configuration
public class AnalysisConfiguration {

  @Bean
  public AnalysisService analysisService(){
    return new AnalysisServiceImpl();
  }
  
  @Bean
  public WebResourceProvider webViewDoc(){
    return new WebResourceProvider();
  }
  
  @Bean
  public TilesConfigurer tilesConfigurer() {
	  final TilesConfigurer configurer = new TilesConfigurer();
	  configurer.setDefinitions(new String[] {"WEB-INF/tiles/tiles.xml"});
	  configurer.setCheckRefresh(true);
	  return configurer;
  }
  
  @Bean
  public TilesViewResolver tilesViewResolver() {
	  final TilesViewResolver resolver = new TilesViewResolver();
	  resolver.setViewClass(TilesView.class);
	  return resolver;
  }
  
  /*@Bean
  public UrlBasedViewResolver viewResolver() {
      UrlBasedViewResolver tilesViewResolver = new UrlBasedViewResolver();
      tilesViewResolver.setViewClass(TilesView.class);
      return tilesViewResolver;
  }

  @Bean
  public TilesConfigurer tilesConfigurer() {

      TilesConfigurer tiles = new TilesConfigurer();
      tiles.setDefinitions(new String[] { "/WEB-INF/tiles/tiles.xml" });
      return tiles;
  }*/


}