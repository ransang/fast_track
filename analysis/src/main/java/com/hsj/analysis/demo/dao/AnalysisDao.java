package com.hsj.analysis.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hsj.analysis.demo.domain.BbsDomain;

@Mapper
public interface AnalysisDao {
	public List<BbsDomain> getBbsList();
}

